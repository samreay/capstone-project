// Tutorial followed from <a href="http://www.html5rocks.com/en/tutorials/canvas/notearsgame/">No Tears Game</a>

//========================================================
// EVERYTHING NEEDED TO UNDERSTAND THE SIMULATION IS BELOW
//========================================================

var FPS = 30;
var steps = 100;
var sim = {
    position : 50, // Displacement in m
    velocity : 0, // Velocity in m/s
    mass : 10, // Mass in kg
    spring : 10, // Spring constant in N/m
    gravity : 9.81, // Gravity strength in m/s^2
    timeRatio : 1, // Speed of time
    vertical : true,
    startingPosition: 50

};

/**
 * Updates the simulation
 */
function update() {
    var force = 0;
    var acceleration = 0;
    // To make the simulation smoother, we must break it into lots of tiny steps.
    for (var i = 0; i < steps; i++) {
        force = -1 * sim.position * sim.spring - (sim.vertical ? sim.gravity * sim.mass : 0);
        acceleration = force / sim.mass;
        sim.velocity += sim.timeRatio * acceleration / (FPS * steps);
        sim.position += sim.timeRatio * sim.velocity / (FPS * steps);
    }
    // Store the values to output them in the graph
    if (sim.timeRatio > 0) {
        positions.set(sim.position);
        velocities.set(sim.velocity);
        accelerations.set(acceleration);
        springEnergy.set(sim.position * sim.position * 0.5 * sim.spring);
        kineticEnergy.set(sim.velocity * sim.velocity * 0.5 * sim.mass);
        gravitational.set(sim.vertical ? sim.mass * sim.position * sim.gravity : 0);
        totalEnergy.set(gravitational.getLast() + kineticEnergy.getLast() + springEnergy.getLast());
    }
}

//========================================================
// END SIMULATION. BEGIN TECHNICAL AND BORING CODING FUN!!
//========================================================

// GLOBAL VARIABLES
var parentDivId = "applet";
var leftWrapper;
var simbox = {
    canvasElement : null,
    canvas : null,
    height : 250,
    width : 300,
    border : 10,
    x : 0,
    y : 0
};

var graphbox = {
    canvasElement : null,
    canvas : null,
    height : 500,
    width : 300,
    ballRadius : 2,
    border : 0,
    x : 0,
    y : 0,
    box1 : {
        x : 30,
        y : 30,
        height : 200,
        width : 250
    },
    box2 : {
        x : 30,
        y : 270,
        height : 220,
        width : 250
    }
};


var numberRecords = graphbox.box1.width;
var positions = new CircularArray(numberRecords, "#089E19");
var velocities = new CircularArray(numberRecords, "#E80707");
var accelerations = new CircularArray(numberRecords, "#E08A00");
var springEnergy = new CircularArray(numberRecords, "#089E19");
var kineticEnergy = new CircularArray(numberRecords, "#E80707");
var gravitational = new CircularArray(numberRecords, "#4098D6");
var totalEnergy = new CircularArray(numberRecords, "#555");
var updateRequested = false;
var updateLines = new Array(20);

/**
 * Creates the canvas element, context, appends to the HTML div with id 'applet' and starts simulation
 */
$(document).ready(function() {

    createInputForm()
    createSimulationCanvas();
    createGraphCanvas()
    // Set updates
    setInterval(function() {
        update();
        draw();
    }, 1000 / FPS);

})

var backup = 0;
function pauseSimulation() {
    if (sim.timeRatio > 0) {
        $("#appletPauseButton").val("Unpause");
        backup = sim.timeRatio;
        sim.timeRatio = 0;
    } else {
        sim.timeRatio = backup;
        $("#appletPauseButton").val("Pause");
    }
}

function updateSimulation() {
    var orientation = $('#appletOrientationControl').val();
    if (orientation == "v") {
        sim.vertical = true;
    } else {
        sim.vertical = false;
    }
    
    var springVal = $("#appletSpringControl").val();
    if (springVal == 0 || springVal == null) {
        //TODO Make defaults properly
    } else {
        sim.spring = springVal;
    }
    
    var massVal = $("#appletMassControl").val();
    if (massVal == 0 || massVal == null) {
        //TODO Make defaults properly
    } else {
        sim.mass = massVal;
    }
    
    var gravVal = $("#appletGravControl").val();
    if (gravVal == null) {
        //TODO Make defaults properly
    } else {
        sim.gravity = gravVal;       
    }
    
    var timeVal = $("#appletTimeControl").val();
    if (timeVal == 0 || timeVal == null) {
        $("#appletTimeControl").val(1)
    } else {
        sim.timeRatio = timeVal;
        backup = timeVal;
    }
    
    sim.position = sim.startingPosition;
    sim.velocity = 0;
    updateRequested = true;
}



function createSimulationCanvas() {
    simbox.canvasElement = $("<canvas style=\"float:left\" width='" + simbox.width + "' height='" + simbox.height + "'></canvas>");
    simbox.canvas = simbox.canvasElement.get(0).getContext("2d");
    leftWrapper.append(simbox.canvasElement);
    div = $(document.createElement('div')).attr({style : 'clear: both'});
    leftWrapper.append(div);
}

function createInputForm() {
    
    leftWrapper = $('<div id="rightAppletWrapper" style="float:left; width: ' + graphbox.width + 'px"></div>')
    var helperDiv = $('<div id="appletInputDiv" style="height: ' + (graphbox.height - simbox.height) + 'px; width: ' + graphbox.width + 'px;"></div>')
    $("#" + parentDivId).append(leftWrapper);
    leftWrapper.append(helperDiv)
    
    var heading = document.createElement('h2');
    heading.innerHTML = 'Configure Simulation...';
    helperDiv.append(heading);
    
    var formElement = $('<form id="appletControlForm"></form>');
    helperDiv.append(formElement);
    
    var orientationLabel = $(document.createElement('label')).attr({id: 'appletOrientationLabel'}).text('Orientation:');
    var orientationInput = $(document.createElement('select')).attr({id: 'appletOrientationControl', name: 'orientation'});
    var vertical = $(document.createElement('option')).attr({value: 'v'}).text('Vertical');
    var horizontal = $(document.createElement('option')).attr({value: 'h'}).text('Horizontal');

    vertical.appendTo(orientationInput);
    horizontal.appendTo(orientationInput);
    formElement.append(orientationLabel);
    formElement.append(orientationInput);
    
    var springLabel = $(document.createElement('label')).attr({id: 'appletSpringLabel'}).text('Spring Constant(N/m):');
    var springInput = $('<input id="appletSpringControl" type="text" name="spring" />');
    springInput.val(sim.spring);
    formElement.append(springLabel);
    formElement.append(springInput);
    
    var massLabel = $(document.createElement('label')).attr({id: 'appletMassLabel'}).text('Mass (kg):');
    var massInput = $('<input id="appletMassControl" type="text" name="mass" />');
    massInput.val(sim.mass);
    formElement.append(massLabel);
    formElement.append(massInput);
    
    var gravLabel = $(document.createElement('label')).attr({id: 'appletGravLabel'}).text('Gravitational Force (N/kg):');
    var gravInput = $('<input id="appletGravControl" type="text" name="gravity" />');
    gravInput.val(sim.gravity);
    formElement.append(gravLabel);
    formElement.append(gravInput);
    
    var timeLabel = $(document.createElement('label')).attr({id: 'appletTimeLabel'}).text('Simulation Speed:');
    var timeInput = $('<input id="appletTimeControl" type="text" name="time" />');
    timeInput.val(sim.timeRatio.toFixed(2));
    formElement.append(timeLabel);
    formElement.append(timeInput);
    

    var submitElement = $('<input type="submit" id="appletUpdateButton" value="Update">');
    $(function() {  
        $("#appletUpdateButton").click(function() {  
            updateSimulation();
            return false;
        });  
    });  
    formElement.append(submitElement);
    var pauseElement = $('<input type="button" id="appletPauseButton" value="Pause">');
    $(function() {
        $("#appletPauseButton").click(function() {
            pauseSimulation();
            return false;
        });
    });
    formElement.append(pauseElement);
}

function createGraphCanvas() {
    graphbox.canvasElement = $("<canvas style=\"float:left\" width='" + graphbox.width + "' height='" + graphbox.height + "'></canvas>");
    graphbox.canvas = graphbox.canvasElement.get(0).getContext("2d");
    $("#" + parentDivId).append(graphbox.canvasElement);
}

/**
 * Draws the applet in question. Will call to sub methods for each module of the applet.
 */
function draw() {

    drawSimulation();
    drawGraphs();

}

function drawSimulation() {
    var c = simbox.canvas;
    c.fillStyle = "#FFF";
    c.strokeStyle = "#000";
    c.fillRect(0, 0, simbox.width, simbox.height);
    c.fillStyle = "#EEE";
    insetRect(c, simbox, true);
    c.fillStyle = "#555";
    insetRect(c, simbox, false);
    if (sim.vertical) {
        var r = (simbox.height - 2 * simbox.border) / ((simbox.height / 2) - sim.position);
        if (r > 1) {
            r = 1;
        }
        var springStart = {
            x : (simbox.width / 2),
            y : simbox.border
        };
        drawGrid(c,50,50,r);
        var springEnd = {x : springStart.x, y : ((simbox.height / 2) - sim.position) * r};
        drawSpring(c, springStart, springEnd, r, true);
        c.beginPath();
        c.arc(springStart.x, springStart.y, 5, 0, 2 * Math.PI, false);
        c.fillStyle = "#000";
        c.fill();
        c.beginPath();
        c.arc(springEnd.x, springEnd.y, 10 * r, 0, 2 * Math.PI, false);
        c.fillStyle = "#1856AD";
        c.fill();
            
        
    } else {
        var springStart = {x: simbox.width / 2, y: simbox.height / 2};
        c.beginPath();
        c.arc(springStart.x, springStart.y, 5, 0, 2 * Math.PI, false);
        c.fillStyle = "#000";
        c.fill();
        
        var springEnd = {x: springStart.x + sim.position * 2, y: springStart.y};
        drawGrid(c,50,50,1);

        drawSpring(c, springStart, springEnd, 1, false);
        
        c.fillStyle = "#777";
        c.fillRect(simbox.border + 1, simbox.height / 2 + 10, simbox.width - 2 * simbox.border - 2, (simbox.height - 2 * simbox.border) / 2 - 11);
        
        c.beginPath();
        c.arc(springEnd.x, springEnd.y, 10, 0, 2 * Math.PI, false);
        c.fillStyle = "#1856AD";
        c.fill();
    }

}

function drawGrid(c, x, y, ratio) {
    c.strokeStyle = "#888";
    c.lineWidth = 0.5;
    c.beginPath();
    for (var i = 0; i < (simbox.width - 2*simbox.border) / 2; i += x * ratio) {
        c.moveTo(simbox.width / 2 + i, simbox.border);
        c.lineTo(simbox.width / 2 + i, simbox.height - simbox.border);
        c.moveTo(simbox.width / 2 - i, simbox.border);
        c.lineTo(simbox.width / 2 - i, simbox.height - simbox.border);
    }
    
    var mid = (simbox.height / 2) * ratio;
    var top = mid;
    var bottom = mid;
    while (top > 0) {
        c.moveTo(simbox.border, top);
        c.lineTo(simbox.width - simbox.border, top);
        top -= y * ratio;
    }
    while (bottom < simbox.height - simbox.border) {
        c.moveTo(simbox.border, bottom);
        c.lineTo(simbox.width - simbox.border, bottom);
        bottom += y * ratio;
    }
    c.stroke();
    c.beginPath();
    c.strokeStyle = "#000";
    c.font = 'Italic 11px Sans-Serif';
    if (sim.vertical) {
        c.fillText('x=0', simbox.border + 10, mid + 12);
        c.moveTo(simbox.border, (simbox.height / 2) * ratio);
        c.lineTo(simbox.width - simbox.border, (simbox.height / 2) * ratio);
    } else {
        c.fillText('x=0', simbox.width / 2 + 5, simbox.border + 12);
        c.moveTo(simbox.width / 2, simbox.border);
        c.lineTo(simbox.width / 2, simbox.height - simbox.border);
    }
    c.stroke();
}

function drawGraphs() {
    var c = graphbox.canvas;
    c.strokeStyle = "#000";
    c.fillStyle = "#FFF";
    c.fillRect(0, 0, graphbox.width, graphbox.height);
    c.fillStyle = "#EEE";
    if (updateRequested) {
        updateRequested = false;
        for (var i = 0; i < updateLines.length; i++) {
            if (updateLines[i] == 0 || isNaN(updateLines[i])) {
                updateLines[i] = numberRecords;
                break;
            }
        }
    }
    
    
    c.fillStyle = "#111";
    c.font = 'Bold 13px Sans-Serif';
    c.fillText('Spatial coordinates', graphbox.box1.x, graphbox.box1.y - 10);
    c.font = '12px Sans-Serif';
    
    c.fillStyle = positions.getColour();
    c.fillText('Position', graphbox.box1.x, graphbox.box1.y + 10);
    c.fillStyle = velocities.getColour();
    c.fillText('Velocity', graphbox.box1.x + 60, graphbox.box1.y + 10);
    c.fillStyle = accelerations.getColour();
    c.fillText('Acceleration', graphbox.box1.x + 115, graphbox.box1.y + 10);
    
    c.fillStyle = springEnergy.getColour();
    c.fillText('Elastic Potential', graphbox.box2.x, graphbox.box2.y - 3);
    
    c.fillStyle = kineticEnergy.getColour();
    c.fillText('Kinetic', graphbox.box2.x + 150, graphbox.box2.y - 3);
    
   c.fillStyle = gravitational.getColour();
    c.fillText('Gravitational Potential', graphbox.box2.x, graphbox.box2.y + 14);
    
    c.fillStyle = totalEnergy.getColour();
    c.fillText('Total', graphbox.box2.x + 150, graphbox.box2.y + 14);
    
    c.fillStyle = "#111";
    c.font = 'Bold 13px Sans-Serif';
    c.fillText('Energy', graphbox.box2.x, graphbox.box2.y - 20);
    
    
    // Draw seperators
    c.strokeStyle = "#555";
    for (var i = 0; i < updateLines.length; i++) {
        if (updateLines[i] != 0 && !isNaN(updateLines[i])) {
            var x = graphbox.box1.x + updateLines[i] - 1;
            c.beginPath();
            c.moveTo(x, graphbox.box1.y + graphbox.box1.height * 0.1);
            c.lineTo(x, graphbox.box1.height + graphbox.box1.y)
            c.stroke();
            c.beginPath();
            c.moveTo(x, graphbox.box2.y + graphbox.box2.height * 0.1);
            c.lineTo(x, graphbox.box2.height + graphbox.box2.y)
            c.stroke();
            if (sim.timeRatio > 0) {
                updateLines[i]--;
            }
        }
    }

    strokePath(c, [positions, velocities, accelerations], graphbox.box1, true);
    strokePath(c, [springEnergy, kineticEnergy, gravitational, totalEnergy], graphbox.box2, false);

}

function strokePath(c, data, box, position) {
    var max = 0;
    var min = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i].getMax() > max) {
            max = data[i].getMax();
        }
        if (data[i].getMin() < min) {
            min = data[i].getMin();
        }
    }
    var r = (box.height * 0.75) / (max - min);
    var offset = r * max + (box.height * 0.15);
    // Draw axes
    c.strokeStyle = "#111";
    c.beginPath();
    c.moveTo(box.x, box.y + offset);
    c.lineTo(box.x + numberRecords, box.y + offset);
    c.moveTo(box.x, box.y + box.height * 0.10);
    c.lineTo(box.x, box.y + box.height);
    c.stroke();
    // Draw triangle
    c.beginPath();
    c.moveTo(box.x + numberRecords + 6, box.y + offset);
    c.lineTo(box.x + numberRecords, box.y + offset - 3);
    c.lineTo(box.x + numberRecords, box.y + offset + 3);
    c.fill();
    c.moveTo(box.x, box.y + box.height * 0.10);
    c.lineTo(box.x - 3, box.y + 6 + box.height * 0.10);
    c.lineTo(box.x + 3, box.y + 6 + box.height * 0.10);
    c.fill();
    c.moveTo(box.x, box.y + box.height);
    c.lineTo(box.x - 3, box.y - 6 + box.height);
    c.lineTo(box.x + 3, box.y - 6 + box.height);
    c.fill();
    c.font = 'Italic 11px Sans-Serif';
    c.fillText('t', box.x + numberRecords + 6, box.y + offset + 10);
    c.fillStyle = "#777";
    if (position) {
        if (sim.vertical) {
            c.fillText('Upwards', box.x + 5, box.y + 8 + (box.height * 0.1));
            c.fillText('Downwards', box.x + 5, box.y + box.height);
        } else {
            c.fillText('Leftwards', box.x + 5, box.y + 8 + (box.height * 0.1));
            c.fillText('Rightwards', box.x + 5, box.y + box.height);
        }
    } else {
        c.fillText('Positive', box.x + 5, box.y + 8 + (box.height * 0.1));
        c.fillText('Negative', box.x + 5, box.y + box.height);
    }
    for (var j = 0; j < data.length; j++) {
        c.strokeStyle = data[j].getColour();
        c.beginPath();
        for (var i = 0; i < numberRecords; i++) {
            var x = box.x + i;
            var y = box.y + offset - (data[j].get(i) * r);
            if (i == 0) {
                c.moveTo(x, y);
            } else {
                c.lineTo(x, y);
            }
        }
        c.stroke();
        c.beginPath();
        c.arc(box.x + numberRecords, box.y + offset - (data[j].getLast() * r), graphbox.ballRadius, 0, 2 * Math.PI, false);
        c.fillStyle = shadeColor(data[j].getColour(), -20);
        c.fill();
        c.fillStyle = "#111";
        c.font = '12px Sans-Serif';
        c.fillText('0', box.x - 12, box.y + offset + 5);
    }
}

function drawSpring(c, start, end, ratio, vertical) {
    c.strokeStyle = "#089E19";
    if (vertical) {
        end.y = end.y - 10;
    }
    c.lineWidth = 2;
    c.beginPath();
    c.moveTo(start.x, start.y);
    var steps = vertical ? end.y - start.y : end.x - start.x;
    for (var i = 0; i < Math.abs(steps); i++) {
        var p = 6 * ratio * Math.sin((16 * Math.PI / steps) * i);
        if (vertical) {
            c.lineTo(start.x + p, start.y + i);
        } else {
            c.lineTo(start.x + (i * Math.abs(steps) / steps), start.y + p);
        }
    }
    
    c.stroke();
}

function insetRect(c, box, fill) {
    if (fill) {
        c.fillRect(box.x + box.border, box.y + box.border, box.width - (2 * box.border), box.height - (2 * box.border));
    } else {
        c.strokeRect(box.x + box.border, box.y + box.border, box.width - (2 * box.border), box.height - (2 * box.border));
    }
}

//========================================================
// TECHNICAL DATA STRUCTURES AND ALGORITHMS WILL GO BELOW.
//========================================================

function shadeColor(color, percent) {   
    var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, B = (num >> 8 & 0x00FF) + amt, G = (num & 0x0000FF) + amt;
    return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
}

/**
 * Creates a circular array of the specified length that has only a
 * simple set function which appends the value onto the end of the array.
 *
 * @param {Number} n the length of the circular array
 */
function CircularArray(n, c) {
    this.array = new Array(n);
    this.last = 0;
    
    this.max = 0;
    this.maxIndex = 0;
    
    this.min = 0;
    this.minIndex = 0;
    
    this.colour = c;
};
CircularArray.prototype.getColour = function() {
    return this.colour;
}
CircularArray.prototype.toString = function() {
    return '[Circular array of length ' + this.array.length + ' and current index ' + this.last + ']';
};
CircularArray.prototype.get = function(i) {
    var index = (this.last + i + 1) % this.array.length;
    return this.array[index];
};
CircularArray.prototype.set = function(v) {
    this.last = (this.last + 1) % this.array.length;
    this.array[this.last] = v;
    if (v > this.max) {
        this.max = v;
        this.maxIndex = this.last;
    } else if (this.last == this.maxIndex) {
        this.max = 0;
        for (var i = 0; i < this.array.length; i++) {
            if (this.array[i] > this.max) {
                this.max = this.array[i];
                this.maxIndex = i;
            }
        }
    }
    if (v < this.min) {
        this.min = v;
        this.minIndex = this.last;
    } else if (this.last == this.minIndex) {
        this.min = 0;
        for (var i = 0; i < this.array.length; i++) {
            if (this.array[i] < this.min) {
                this.min = this.array[i];
                this.minIndex = i;
            }
        }
    }
};
CircularArray.prototype.getLast = function() {
    return this.array[this.last];
}
CircularArray.prototype.getMax = function() {
    return this.max;
}
CircularArray.prototype.getMin = function() {
    return this.min;
}
CircularArray.IndexError = {};


// From a tutorial
$(function() {
  window.keydown = {};
  
  function keyName(event) {
    return jQuery.hotkeys.specialKeys[event.which] ||
      String.fromCharCode(event.which).toLowerCase();
  }
  
  $(document).bind("keydown", function(event) {
    keydown[keyName(event)] = true;
  });
  
  $(document).bind("keyup", function(event) {
    keydown[keyName(event)] = false;
  });
});
